from escpos import *

class POS:
  def __init__(self, print_name):
    self.usb = printer.Usb(0x0519,0x2013, 0, 0x81, 0x03)
    self.out = []

  # code: 1
  def Transmit_Printer_Status(self):
    self.usb.device.write(self.usb.out_ep, "\x10\x04\x01", 5000)
    s = self.usb.device.read(self.usb.in_ep, 256,5000)[0]
    self.out = []
    if ((s & 8) == 0):
      self.out.append({
        "msg": "Offline.",
        "status:": s,
        "bit": 3
      })
    else:
      self.out.append({
        "msg": "Online.",
        "status:": s,
        "bit": 3
      })

    if ((s & 32) == 0):
      self.out.append({
        "msg": "Does not wait for online error recovery.",
        "status:": s,
        "bit": 5
      })
    else:
      self.out.append({
        "msg": "Waits for online error recovery.",
        "status:": s,
        "bit": 5
      })


    if ((s & 64) == 0):
      self.out.append({
        "msg": "FEED switch is Off.",
        "status:": s,
        "bit": 6
      })
    else:
      self.out.append({
        "msg": "FEED switch is On.",
        "status:": s,
        "bit": 6
      })

    self.usb.close()
    return self.out

  # code: 2
  def Transmit_Offline_Status(self):
    self.usb.device.write(self.usb.out_ep, "\x10\x04\x02", 5000)
    s = self.usb.device.read(self.usb.in_ep, 256,5000)[0]
    self.out = []
    if ((s & 4) == 0):
      self.out.append({
        "msg": "Platen is closed.",
        "status:": s,
        "bit": 2
      })
    else:
      self.out.append({
        "msg": "Platen is opened.",
        "status:": s,
        "bit": 2
      })


    if ((s & 8) == 0):
      self.out.append({
        "msg": "Paper is not being fed by FEED button.",
        "status:": s,
        "bit": 3
      })
    else:
      self.out.append({
        "msg": "Paper is being fed by FEED button.",
        "status:": s,
        "bit": 3
      })

    if ((s & 32) == 0):
      self.out.append({
        "msg": "No paper-end stop.",
        "status:": s,
        "bit": 5
      })
    else:
      self.out.append({
        "msg": "Printing is being stopped.",
        "status:": s,
        "bit": 5
      })
    if ((s & 64) == 0):
      self.out.append({
        "msg": "No error.",
        "status:": s,
        "bit": 6
      })
    else:
      self.out.append({
        "msg": "Error occurred.",
        "status:": s,
        "bit": 6
      })
    self.usb.close()
    return self.out

  # code: 3
  def Transmit_Error_Status(self):
    self.usb.device.write(self.usb.out_ep, "\x10\x04\x03", 5000)
    s = self.usb.device.read(self.usb.in_ep, 256,5000)[0]
    self.out = []
    if ((s & 4) == 0):
      self.out.append({
        "msg": "No mechanical error.",
        "status:": s,
        "bit": 2
      })
    else: 
      self.out.append({
        "msg": "mechanical error has occurred.",
        "status:": s,
        "bit": 2
      })
    if ((s & 8) == 0):
      self.out.append({
        "msg": "No auto cutter error.",
        "status:": s,
        "bit": 3
      })
    else: 
      self.out.append({
        "msg": "auto cutter error has occurred.",
        "status:": s,
        "bit": 3
      })

    if ((s & 32) == 0):
      self.out.append({
        "msg": "No unrecoverable error.",
        "status:": s,
        "bit": 5
      })
    else: 
      self.out.append({
        "msg": "unrecoverable error has occurred.",
        "status:": s,
        "bit": 5
      })
    if ((s & 64) == 0):
      self.out.append({
        "msg": "No auto-recoverable error.",
        "status:": s,
        "bit": 6
      })
    else: 
      self.out.append({
        "msg": "auto-recoverable error has occurred.",
        "status:": s,
        "bit": 6
      })
    self.usb.close()
    return self.out
  
  # code: 4
  def Transmit_Paper_Roll_Sensor_Status(self):
    self.usb.device.write(self.usb.out_ep, "\x10\x04\x04", 5000)
    s = self.usb.device.read(self.usb.in_ep, 256,5000)[0]
    self.out = []
    if ((s & 12) == 0):
      self.out.append({
        "msg": "Paper roll end 2 sensor: paper adequate.",
        "status:": s,
        "bit": "2 - 3"
      })
    else: 
      self.out.append({
        "msg": "Paper near-end is detected by the paper roll near-end sensor.",
        "status:": s,
        "bit": "2 - 3"
      })
    if ((s & 96) == 0):
      self.out.append({
        "msg": "paper roll sensor: paper present.",
        "status:": s,
        "bit": "5 - 6"
      })
    else: 
      self.out.append({
        "msg": "paper roll end detected by paper roll sensor.",
        "status:": s,
        "bit": "5 - 6"
      })
    self.usb.close()
    return self.out

  def print_line(self, line):
    for i in range(line):
      self.usb.text('\n')
      
  def cut(self):
    self.usb.cut()
    self.usb.close()

  def print_text(self, content):
    self.usb.text(content)
    self.usb.close()

  def print_qr(self, content, size):
    self.usb.qr(content,3, size, 2)
    self.usb.close()

  def print_barcode(self, content):
    self.usb.barcode(content,'EAN13',64,4,'','')
    self.usb.close()
  def print_image(self, sorces):
    self.usb.image(sorces)
    self.usb.close()

# try:

#   e = printer.Usb(0x0519,0x2013, 0, 0x81, 0x03)

#   # Transmit printer status；
#   e.device.write(e.out_ep, "\x10\x04\x01", 5000)
#   s = e.device.read(e.in_ep, 256,5000)[0]
#   if (s-22) == 0:
#     print("Online.")
#   elif (s-22) == 8:
#     print("Offline.")
#   elif (s-22) == 32:
#     print("Waits for online error recovery")

#   # Transmit offline status；
#   e.device.write(e.out_ep, "\x10\x04\x02", 5000)
#   s = e.device.read(e.in_ep, 256,5000)[0]
#   print(s)
#   if (s-18) == 32:
#     print("Printing is being stopped.")
#   elif (s-18) == 0:
#     print("Printing ready.")
#   elif (s-18) == 32:
#     print("Waits for online error recovery")

#   # Transmit paper roll sensor status；
#   e.device.write(e.out_ep, "\x10\x04\x04", 5000)
#   s = e.device.read(e.in_ep, 256,5000)[0]
#   if s == 28:
#     print("Paper low.")
#   elif s == 112:
#     print("Paper out.")
#   elif s == 16:
#     print("Printer Ready.")
#   else:
#     print("unknow.")

#   ## close usb.
#   e.close()
# except expression as identifier:
#   print(identifier)