if __name__ == "__main__":
  out = []
  try:
    from app import aot_pos_app
    import argparse

    App = aot_pos_app.POS('Star Micronics Co., Ltd')

    parser = argparse.ArgumentParser()
    parser.add_argument("--status", help="get status printer. [ 1 - 4]")
    parser.add_argument("--write", help="write paper. [ Text ]")
    parser.add_argument("--ln", help="new line. [ Number ]")
    parser.add_argument("--cut", help="cut paper. [ True, False ]")
    parser.add_argument("--qr", help="write Qr Code. [ Text ]")
    parser.add_argument("--barcode", help="write BarCode (13 chart). [ Text ]")
    parser.add_argument("--image", help="write image. [ Text ]")
    args = parser.parse_args()

    if (args.status != None) :
      if (args.status == '1'):
        out = App.Transmit_Printer_Status()
        print(out)
      elif (args.status == '2'):
        out = App.Transmit_Offline_Status()
        print(out)
      elif (args.status == '3'):
        out = App.Transmit_Error_Status()
        print(out)
      elif (args.status == '4'):
        out = App.Transmit_Paper_Roll_Sensor_Status()
        print(out)
    if (args.write != None):
      App.print_text(args.write)
    if (args.ln != None):
      App.print_line(int(args.ln))
    if (args.cut != None):
      if (args.cut == 'true'):
        App.cut()
        print ("OK.")
      elif (args.cut == 'false'):
        print ("OK.")
    if (args.qr != None):
      App.print_qr(args.qr, int(16))
    if (args.barcode != None):
      App.print_barcode(args.barcode)
    if (args.image != None):
      App.print_image(args.image)

  except Exception as e:
    out.append({
      "error": e
    })
    print(out)